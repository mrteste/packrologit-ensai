
# logitUnivar -------------------------------------------------------------

#' \code{logitUniv}
#'
#' @description Computes the value of logit univariate function.
#' @usage logitUniv(x, beta)
#' @param x \code{numeric} vector
#' @param beta \code{numeric} vector of length one
#' @return Returns a \code{numeric} vector
#' @examples
#' logitUniv(x = 1:10, beta = 0.5)
#' @export
logitUniv <- function(x, beta){
  return(exp(beta*x)/(1 + exp(beta*x)))
}

# simulationMulti -------------------------------------------------------------

#' \code{simulationUniv}
#'
#' @description Generates a sample of n elements with a standardised normal distribution, and a sample of n elements
#' with a logit distribution.
#' @usage simulationUniv(n, beta)
#' @param n Size of the sample (\code{integer})
#' @param beta Coefficient of the logit function - \code{numeric} vector of length one
#' @return Returns a \code{list} containing :
#' \itemize{
#'   \item \code{echant} A \code{matrix} of n rows and 2 columns.
#'   \item \code{beta} The same as input.
#' }
#' @examples
#' echantillon <- simulationUniv(100, 0.5)
#' @export
simulationUniv <- function(n, beta){
  X <- stats::rnorm(n)
  proba_Y_1 <- logitUniv(X, beta)
  Y <- stats::rbinom(n, size = 1, prob = proba_Y_1)
  echant <- cbind(X,Y)
  colnames(echant) <- c("x","y")
  return(list("echant" = echant, "beta" = beta))
}


# visualisationUniv -------------------------------------------------------------

#' \code{visualisationUniv}
#'
#' @description Visualisation
#' @usage visualisationUniv(X, beta)
#' @param X \code{Numeric} vector
#' @param beta Coefficient of the logit function to test - \code{numeric} vector of length one
#' @return Prints a \code{list} containing :
#' \itemize{
#'   \item \code{echant} A \code{matrix} of n rows and 2 columns.
#'   \item \code{beta} The same as input.
#' }
#' @examples
#' visualisationUniv(rnorm(100), 0.5)
#' visualisationUniv(rnorm(100), c(0, 0.1, 0.5, 2, 5, 10))
#' @export
visualisationUniv <- function(X,beta){
  n <- length(beta)
  nx <- length(X)
  if(n>9) beta <- sort(beta)[1:9] else beta <- sort(beta)
  proba_Y_1 <- sapply(beta, function(b) logitUniv(X, b))
  Y <- Reduce(cbind,Map(function(b) stats::rbinom(nx, size = 1, prob = proba_Y_1[,b]), 1:n))
  BY <- sort(rep(beta,nx))
  data_points <- data.frame(
    x1 = rep(X,n),
    y1 = Reduce(rbind, Y),
    beta = as.factor(Reduce(rbind, BY)),
    row.names = 1:(n*nx)
  )
  distrib <- function(b, points = data_points){

    ggplot2::ggplot(data = points, ggplot2::aes_string(x = 'x1')) +
      ggplot2::geom_point(data=points[points$beta == b,],ggplot2::aes_string(y = 'y1'),shape = 21,
                          alpha = 1, col="steelblue") +
      ggplot2::stat_function(fun = logitUniv, args = list(beta = b), col="steelblue") +
      ggplot2::scale_y_continuous(limits = c(0,1)) +
      ggplot2::ggtitle(bquote("Distribution logit pour " ~ beta ~ "=" ~ .(b))) +
      ggplot2::theme_classic()
  }
  graphs <- Map(distrib, beta)
  gridExtra::marrangeGrob(graphs,
                          ncol = ifelse(n==1,1,2),
                          nrow = ifelse(n==1,1,ceiling(n/2)),
                          top = ""
  )
}

# predictionUniv -------------------------------------------------------------
#' \code{predictionUniv}
#'
#' @description Computes estimates of probabilities from a logit regression done with \link[regLog:estimationUniv]{estimationUniv}.
#' @usage predictionUniv(res_log, seuil = 0.5)
#' @param res_log An object of class \link[regLog:RegLog-class]{RegLog}
#' @param seuil A length-one \code{numeric} vector, between 0 and 1
#' @return Returns (computes if necessary) a list containing the estimated probabilities and estimated values for the explained variable.
#' @examples
#' simul <- simulationUniv(1000,0.5)
#' reg_univ <- estimationUniv(simul$echant)
#' y_predict_univ <- predictionUniv(reg_univ)
#' @export
predictionUniv <- function(res_log, seuil = 0.5){
  stopifnot(methods::is(object = res_log, class2 = "RegLog"))
  p_logit_predict <- logitUniv(res_log@echant[,"x"], c(res_log@betas_estim))
  Y_predict <- ifelse(p_logit_predict>=seuil, 1, 0)
  return(list(y = Y_predict, probas = p_logit_predict))
}

# statbetaUniv -------------------------------------------------------------
#' \code{statbetaUniv}
#'
#' @description Returns p-value and confidence interval for beta estimated by \link[regLog:estimationUniv]{estimationUniv}
#' @usage statbetaUniv(res_log, confidence = 0.95)
#' @param res_log An object of class \link[regLog:RegLog-class]{RegLog}
#' @param confidence A length-one \code{numeric} vector, between 0 and 1
#' @return Returns (computes if necessary) a list containing the p-value and confidence interval.
#' @seealso \link[regLog:estimationUniv]{estimationUniv}
#' @examples
#' simul <- simulationUniv(1000,0.5)
#' reg_univ <- estimationUniv(simul$echant)
#' stat_univ <- statbetaUniv(reg_univ)
#' @export
statbetaUniv <- function(res_log, confidence = 0.95){
  beta_estim <- c(res_log@betas_estim)
  # log_vrais_der_sec <- function(beta) -sum(res_log@echant[,"x"]^2 * exp(res_log@echant[,"x"]*beta)/(1+exp(res_log@echant[,"x"]*beta))^2)
  var_estim <- res_log@vars_estim #-1/log_vrais_der_sec(beta_estim)
  z_value <- abs(beta_estim)/sqrt(var_estim)
  ddl <- nrow(res_log@echant)-1
  p_value <-  2*(1- stats::pt(q = z_value, df = ddl))
  alpha <- 1 - confidence
  confinterval <- matrix(c(beta_estim - sqrt(var_estim) * stats::qt(p = 1-alpha/2, df = ddl),
                    beta_estim + sqrt(var_estim) * stats::qt(p = 1-alpha/2, df = ddl)),ncol = 2)
  colnames(confinterval) <- as.character(c(alpha/2, 1-alpha/2))
  return(list(pvalue = p_value, confint = confinterval))
}

# estimationUniv -------------------------------------------------------------
#' \code{estimationUniv}
#'
#' @description Computes estimates of probabilities from a \link[regLog:RegLog-class]{RegLog} object.
#' @usage estimationUniv(echant, init = NULL, seuil = 0.00001, nbIter = 20, nbTests = 100)
#' @param echant A \code{matrix} of n rows and 2 columns, the last one is the binary variable to explain
#' and the first one represents the explicative variable.
#' @param init A numeric for initialising the algorithm or
#'  \code{NULL} (by default) in order to let the function acting by itself
#' @param seuil Precision of the approximation. A length-one \code{numeric} vector.
#' @param nbIter Max number of iterations for one launching of \link[regLog:newton_raphson]{newton_raphson}  function.
#' @param nbTests Max number of \link[regLog:newton_raphson]{newton_raphson} function launching.
#' @return Returns NULL if the algorithm failed to converge else a list containing
#' \itemize{
#'   \item echant : the input sample
#'   \item res : the result of the successful launching of newton_raphson algorithm
#'   \item nb_tests : number of tests necessary for a successful launching of newton_raphson algorithm
#'   \item beta_init : initial point used for the successful launching of newton_raphson algorithm
#' }
#' @seealso \link[regLog:newton_raphson]{newton_raphson}
#' @examples
#' #On a sample made by simulationMulti() function
#' echantillon <- simulationUniv(200,0.5)
#' resultat <- estimationUniv(echant = echantillon$echant)
#' #On partial ucla data:
#' data(ucla)
#' res_ucla <- estimationUniv(echant = as.matrix(ucla[,c(2,1)]))
#' @export
estimationUniv <- function(echant, init = NULL, seuil = 0.00001, nbIter = 20, nbTests = 100){
  debut <- Sys.time()
  colnames(echant) <- c("x","y")
  log_vrais <- function(beta) sum(beta * echant[,"x"] * echant[,"y"] - log(1+ exp(echant[,"x"]*beta)))
  log_vrais_der <- function(beta) sum(echant[,"x"] * (echant[,"y"] - logitUniv(echant[,"x"],beta)))
  log_vrais_der_sec <- function(beta) -sum(echant[,"x"]^2 * exp(echant[,"x"]*beta)/(1+exp(echant[,"x"]*beta))^2)
  ok <- FALSE
  nTests <- 0
  while(ok == FALSE & nTests < nbTests){
    if(!is.null(init)){
      beta0 <- init
      nTests <- nbTests
    }else{
      betas <- replicate(1000, stats::runif(1,-1,1), simplify = TRUE)
      beta0 <- betas[which.max(Map(log_vrais, betas))]
    }
    res <- newton_raphson(log_vrais_der, log_vrais_der_sec, beta0, seuil, nbIter)
    ok <- is.list(res)
    nTests <- nTests + 1
  }
  if(is.null(tryCatch(res$iterations,
                      error = function(e){
                        return(NULL)
                      }))){
    print("The algorithm didn't converged. The initial point may be too far from the real point")
    return(NULL)
  }else{
    likelihood <- unlist(Map(f = log_vrais, res$iterations))
    beta_estim <- cbind(res$zero)
    rownames(beta_estim) <- "beta"
    resultat <- methods::new("RegLog",
                             betas_estim = beta_estim,
                             vars_estim = -1/log_vrais_der_sec(res$zero),
                             betas_iter = as.matrix(Reduce(cbind, res$iterations)),
                             y_obs = as.integer(echant[,"y"]),
                             aic = -2*likelihood[res$nb_iter] + 2,
                             bic = -2*likelihood[res$nb_iter] + log(nrow(echant)),
                             log_vrais = likelihood,
                             beta_init = matrix(beta0),
                             nb_iter = res$nb_iter,
                             nb_tests = nTests,
                             duree_prog = as.numeric(Sys.time() - debut),
                             echant = echant,
                             taille = nrow(echant),
                             nb_var = 1
    )
    prediction <- predictionUniv(resultat)
    resultat@y_estim <- prediction[["y"]]
    resultat@probas_estim <- prediction[["probas"]]
    statistiques <- statbetaUniv(resultat)
    resultat@pvalue <- statistiques[["pvalue"]]
    resultat@confint <- statistiques[["confint"]]
    return(resultat)
  }
}


