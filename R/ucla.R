#' @title ucla
#' @description How variables, such as GRE (Graduate Record Exam scores), GPA (grade point average) and prestige of the undergraduate institution,
#'  effect admission into graduate school. The response variable, admit/don't admit, is a binary variable. Hypothetical data.
#'
#' @format A data frame with 400 rows and 4 variables:
#' \describe{
#'   \item{admit}{admission into graduate school, binary variable}
#'   \item{gre}{Graduate Record Exam scores}
#'   \item{gpa}{Grade point average}
#'   \item{rank}{Prestige of the undergraduate institution}
#' }
#' @source \url{https://stats.idre.ucla.edu/r/dae/logit-regression/}
"ucla"
