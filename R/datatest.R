#' @title datatest
#' @description a data to test simple logistic regression
#' @format A data frame with 40 rows and 2 variables:
#' \describe{
#'   \item{x}{age, continuous variable}
#'   \item{y}{bought a death metal album, binary variable}
#' }
#' @source \url{http://perso.ens-lyon.fr/lise.vaudor/realiser-une-regression-logistique-avec-r/}
"datatest"
