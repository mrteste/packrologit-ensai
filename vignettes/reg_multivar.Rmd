---
title: "La régression logistique multiple avec le package RegLog"
author: ""
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r echo = TRUE, include = FALSE}
source(file = "G:/packrologit-ensai/regLog/R/func_newton_raphson.R", encoding = "UTF-8")
source(file = "G:/packrologit-ensai/regLog/R/classe_RegLog.R", encoding = "UTF-8")
source(file = "G:/packrologit-ensai/regLog/R/func_logit_multi.R", encoding = "UTF-8")
```


## Simulation d'un échantillon
```{r}
echantillon <- simulationMulti(100, matrix(c(1,2,5), ncol=1))
str(echantillon)
```

## Régression logistique sur un échantillon 
```{r}
resultat <- estimationMulti(echant = echantillon$echant)
```

### Les informations contenues dans le résultat d'une régression
```{r}
mode(resultat)
methods::showClass("RegLog")
str(resultat)
```

### Les méthodes print, show, plot et summary ont été surchargées
```{r}
print(resultat)
plot(resultat)
summary(resultat)
```

### Comparaison avec la fonction `stats::glm`
```{r}
resultat_glm <- stats::glm(y~x1+x2+x3-1, data = as.data.frame(echantillon$echant), family = "binomial")
knitr::kable(data.frame(reglog = resultat@betas_estim, regglm = resultat_glm$coefficients, init = echantillon$beta))
```


