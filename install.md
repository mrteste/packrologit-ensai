
# Guide de démarrage

Pour utiliser un système de gestion de version comme gitlab, il faut utiliser deux outils additionnels :   

- le site gitlab.com qui héberge le projet  
- l'outil git bash sur Windows qui servira à interagir avec le projet  


## Préparation

- s'inscrire sur gitlab.com  
- installer git pour windows (git bash)  
- ouvrir git bash  
- taper la commande suivante  `ssh-keygen -t ed25519 -C "macle"`

gitbash vous demandera d'entrer un mot de passe : pas besoin, taper deux fois sur entrée

- récupérer votre clé en tapant la commande dans git bash `cat ~/.ssh/id_ed25519.pub | clip`  

Cette commande vous permet de copier la clé dans le presse-papier

- connectez-vous sur gitlab.com avec identifiant et mot de passe, 
- dans votre profil, aller dans settings -> SSH Keys
- coller votre clé SSH dans l'endroit indiqué
- taper la commande suivante dans git bash `cd /x/xx`   

où x est le disque et xx le chemin sur disque vers le répertoire qui contiendra le dossier du projet
Cette commande ne fait changer le répertoire de travail (cd = change directory)

(exemple : perso j'ai choisi de mettre le dossier du projet à la racine d'une clé usb, j'ai donc utilisé la commande `cd /g`

## Pour clôner le projet sur son poste 

Clôner un projet, c'est récupérer l'ensemble du répertoire en ligne pour le déposer à l'identique sur son poste de travail. Pour cela, dans git bash, taper la commande suivante :

```
git clone https://gitlab.com/mrteste/packrologit-ensai.git
```

La commande créera le dossier packrologit-ensai dans le dossier de travail que vous avez mentionné avant avec la commande cd
Il est possible que Windows vous demande vos identifiants à gitlab, tapez les et faites entrer.

Le dossier devrait être clôné dans votre répertoire de travail.

